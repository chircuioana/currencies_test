package com.test.currencies.commons

fun getFlagEmojiForCurrencyCode(currencyCode: String): String {
    val flagOffset = 0x1F1E6
    val asciiOffset = 0x41

    val firstChar = Character.codePointAt(currencyCode, 0) - asciiOffset + flagOffset
    val secondChar = Character.codePointAt(currencyCode, 1) - asciiOffset + flagOffset

    return (String(Character.toChars(firstChar))
            + String(Character.toChars(secondChar)))
}