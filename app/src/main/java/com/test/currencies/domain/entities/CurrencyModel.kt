package com.test.currencies.domain.entities

import com.test.currencies.commons.getFlagEmojiForCurrencyCode
import java.text.DecimalFormat
import java.util.*

const val DEFAULT_CURRENCY = "EUR"

data class CurrencyModel(
    val code: String,
    var value: Double?,
    var rate: Double?
) {

    val currencyDisplayName: String
        get() {
            val locale = Locale.getDefault()
            val currency: Currency = Currency.getInstance(code)
            return currency.getDisplayName(locale)
        }

    val formattedValue: String
        get() {
            value ?: return ""
            val precision = DecimalFormat("0.00")
            val formatted = precision.format(value)
            return if (formatted == "0.00") "" else formatted
        }

    val flagEmoji: String
        get() {
            val currency: Currency = Currency.getInstance(code)
            return getFlagEmojiForCurrencyCode(currency.currencyCode)
        }

    fun convert(selectedValue: Double) {
        rate?.let {
            value = it * selectedValue
        } ?: run {
            value = null
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CurrencyModel

        if (code != other.code) return false
        if (value != other.value) return false
//        if (rate != other.rate) return false

        return true
    }

    override fun hashCode(): Int {
        var result = code.hashCode()
        result = 31 * result + (value?.hashCode() ?: 0)
//        result = 31 * result + (rate?.hashCode() ?: 0)
        return result
    }
}