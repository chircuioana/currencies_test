package com.test.currencies.domain.interactors

import android.util.Log
import com.test.currencies.di.IoDispatcher
import com.test.currencies.domain.commons.ErrorEntity
import com.test.currencies.domain.commons.FlowUseCase
import com.test.currencies.domain.commons.Resource
import com.test.currencies.domain.entities.LatestCurrencyModel
import com.test.currencies.domain.repositories.CurrenciesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.*
import javax.inject.Inject

private const val DELAY_TIME = 1 * 1000L

class GetLatestCurrencyRatesPeriodicallyUseCase @Inject constructor(
    private val repository: CurrenciesRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<GetLatestCurrencyRatesPeriodicallyUseCase.Params, LatestCurrencyModel>(dispatcher) {

    var isRefreshOn = true

    class Params(val baseCurrency: String)

    override fun execute(parameters: Params): Flow<Resource<LatestCurrencyModel>> {
        return flow {
            try {
                emit(Resource.Loading)
                while (isRefreshOn) {
                    emit(
                        repository.fetchLatestCurrency(
                            parameters.baseCurrency.toUpperCase(Locale.getDefault())
                        )
                    )
                    Log.e("DELAY","refreshed")
                    delay(DELAY_TIME)
                }
            } catch (e: Exception) {
                e.printStackTrace()
                emit(Resource.Error(ErrorEntity()))
            }
        }
    }
}