package com.test.currencies.domain.entities

import java.util.*

class LatestCurrencyModel(
    val currency: String,
    val rates: Map<String, Double>
) {
    val allCurrencies: List<String>
        get() {
            val list = mutableListOf(currency)
            rates.mapKeys { (key, _) -> list.add(key) }
            return list
        }

    fun getRateFor(currency: String): Double {
        return rates[currency.toUpperCase(Locale.getDefault())] ?: 1.0
    }
}