package com.test.currencies.domain.commons

interface DomainEntity<T> {
    fun toDomain(): T
}