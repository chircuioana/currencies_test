package com.test.currencies.domain.repositories

import com.test.currencies.domain.commons.Resource
import com.test.currencies.domain.entities.LatestCurrencyModel

interface CurrenciesRepository {
    suspend fun fetchLatestCurrency(baseCurrencyCode: String): Resource<LatestCurrencyModel>
}