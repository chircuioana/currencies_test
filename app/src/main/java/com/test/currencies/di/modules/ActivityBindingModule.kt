package com.test.currencies.di.modules

import com.test.currencies.presentation.MainActivity
import com.test.currencies.di.scopes.ActivityScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            // activity
            MainActivityModule::class,
            // fragments
            CurrenciesModule::class
        ]
    )
    internal abstract fun mainActivity(): MainActivity
}