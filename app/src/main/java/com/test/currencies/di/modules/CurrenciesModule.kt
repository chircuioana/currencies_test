package com.test.currencies.di.modules

import androidx.lifecycle.ViewModel
import com.test.currencies.presentation.features.currencies.CurrenciesFragment
import com.test.currencies.presentation.features.currencies.CurrenciesViewModel
import com.test.currencies.di.scopes.ChildFragmentScoped
import com.test.currencies.di.scopes.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
internal abstract class CurrenciesModule {

    @ChildFragmentScoped
    @ContributesAndroidInjector
    internal abstract fun currenciesFragment(): CurrenciesFragment

    @Binds
    @IntoMap
    @ViewModelKey(CurrenciesViewModel::class)
    abstract fun bindCurrenciesViewModel(viewModel: CurrenciesViewModel): ViewModel
}