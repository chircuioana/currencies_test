package com.test.currencies.di.modules

import android.content.Context
import com.test.currencies.presentation.App
import dagger.Module
import dagger.Provides

@Module
open class AppModule {

    @Provides
    fun provideContext(application: App): Context {
        return application.applicationContext
    }
}