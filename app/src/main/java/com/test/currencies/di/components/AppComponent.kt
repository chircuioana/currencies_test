package com.test.currencies.di.components

import com.test.currencies.data.di.ApiModule
import com.test.currencies.data.di.DataModule
import com.test.currencies.presentation.App
import com.test.currencies.di.modules.ActivityBindingModule
import com.test.currencies.di.modules.AppModule
import com.test.currencies.di.modules.CoroutinesModule
import com.test.currencies.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        AppModule::class,
        CoroutinesModule::class,
        DataModule::class,
        ApiModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: App): AppComponent
    }
}