package com.test.currencies.data

import com.squareup.moshi.Json
import com.test.currencies.domain.entities.CurrencyModel
import com.test.currencies.domain.commons.DomainEntity
import com.test.currencies.domain.entities.LatestCurrencyModel

data class LatestCurrencyResponse(
    @Json(name = "baseCurrency")
    val currencyCode: String,
    @Json(name = "rates")
    val rates: Map<String, Double>
) : DomainEntity<LatestCurrencyModel> {
    override fun toDomain(): LatestCurrencyModel {
        return LatestCurrencyModel(
            currencyCode,
            rates
        )
    }
}