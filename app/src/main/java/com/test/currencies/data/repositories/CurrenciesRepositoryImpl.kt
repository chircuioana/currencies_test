package com.test.currencies.data.repositories

import com.test.currencies.data.sources.CurrenciesApi
import com.test.currencies.domain.commons.Resource
import com.test.currencies.domain.commons.mapToDomain
import com.test.currencies.domain.entities.LatestCurrencyModel
import com.test.currencies.domain.repositories.CurrenciesRepository
import javax.inject.Inject

class CurrenciesRepositoryImpl @Inject constructor(
    private val api: CurrenciesApi
) :
    CurrenciesRepository {
    override suspend fun fetchLatestCurrency(baseCurrencyCode: String): Resource<LatestCurrencyModel> =
        api.getLatestCurrency(baseCurrencyCode)
            .mapToDomain()
}