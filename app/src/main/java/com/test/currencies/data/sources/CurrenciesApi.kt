package com.test.currencies.data.sources

import com.test.currencies.data.LatestCurrencyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrenciesApi {

    @GET("latest")
    suspend fun getLatestCurrency(@Query("base") currency: String): Response<LatestCurrencyResponse>
}