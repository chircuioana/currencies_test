package com.test.currencies.data.di

import com.test.currencies.data.repositories.CurrenciesRepositoryImpl
import com.test.currencies.data.sources.CurrenciesApi
import com.test.currencies.domain.repositories.CurrenciesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun providesMarketingCampaignRepository(
        @Named("currenciesApiDataSource") currenciesApi: CurrenciesApi
    ): CurrenciesRepository {
        return CurrenciesRepositoryImpl(currenciesApi)
    }
}