package com.test.currencies.presentation.features.currencies

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.test.currencies.databinding.FragmentCurrenciesBinding
import com.test.currencies.presentation.commons.BaseFragment
import javax.inject.Inject

class CurrenciesFragment @Inject constructor() : BaseFragment() {

    private lateinit var currenciesAdapter: CurrenciesAdapter
    private val viewModel: CurrenciesViewModel by viewModels { viewModelFactory }

    private var _binding: FragmentCurrenciesBinding? = null
    private val binding get() = _binding ?: throw Exception("Expected binding not null")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.fetchCurrencies()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCurrenciesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupCurrenciesRecyclerView()
        observe()
    }

    override fun onStart() {
        super.onStart()
        viewModel.resumeRefresh()
    }

    override fun onStop() {
        super.onStop()
        viewModel.pauseRefresh()
    }

    private fun setupCurrenciesRecyclerView() = binding.run {
        currenciesAdapter = CurrenciesAdapter(
            onSelectedCurrencyUpdated = {
                viewModel.updateSelectedCurrency(it)
            })
        rvCurrencies.apply {
            adapter = currenciesAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observe() = viewModel.run {
        isLoading.observe(viewLifecycleOwner, Observer {
            //TODO show a loading indicator
        })
        error.observe(viewLifecycleOwner, Observer {
            //TODO a more detailed alert message
            AlertDialog.Builder(requireContext())
                .setTitle("Warning")
                .setMessage("An error occurred")
                .show()
        })
        currencies.observe(viewLifecycleOwner, Observer {
            currenciesAdapter.currencies = it
        })
        selectedCurrency.observe(viewLifecycleOwner, Observer {
            binding.rvCurrencies.scrollToPosition(0)
        })
    }

}