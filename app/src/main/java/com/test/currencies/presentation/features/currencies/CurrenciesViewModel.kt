package com.test.currencies.presentation.features.currencies


import androidx.lifecycle.*
import com.test.currencies.commons.safeCollect
import com.test.currencies.domain.commons.Resource
import com.test.currencies.domain.entities.CurrencyModel
import com.test.currencies.domain.entities.DEFAULT_CURRENCY
import com.test.currencies.domain.entities.LatestCurrencyModel
import com.test.currencies.domain.interactors.GetLatestCurrencyRatesPeriodicallyUseCase
import com.test.currencies.presentation.commons.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class CurrenciesViewModel @Inject constructor(
    private val latestCurrencyRatesUseCase: GetLatestCurrencyRatesPeriodicallyUseCase
) : BaseViewModel() {

    val currencies: LiveData<List<CurrencyModel>>
        get() = _currencies
    private val _currencies = MediatorLiveData<List<CurrencyModel>>()

    val selectedCurrency: LiveData<CurrencyModel>
        get() = Transformations.distinctUntilChanged(
            _selectedCurrency
        )
    private val _selectedCurrency = MutableLiveData<CurrencyModel>()
    private var latestCurrencyRatesJob: Job? = null

    init {
        _currencies.addSource(selectedCurrency) { selectedCurrency ->
            val currencies: ArrayList<CurrencyModel> =
                _currencies.value?.toCollection(ArrayList()) ?: arrayListOf()
            if (currencies.contains(selectedCurrency)) {
                currencies.remove(selectedCurrency)
                currencies.add(0, selectedCurrency)
            }
            currencies.forEachIndexed { index, currencyModel ->
                if (index == 0) {
                    currencyModel.rate = 1.0
                } else {
                    currencyModel.rate = null
                    currencyModel.value = null
                }
            }
            _currencies.postValue(currencies)
            fetchCurrencies()
        }
    }

    fun fetchCurrencies() {
        latestCurrencyRatesJob?.cancel()
        latestCurrencyRatesJob = viewModelScope.launch {
            val selectedCurrency = _selectedCurrency.value?.code ?: DEFAULT_CURRENCY
            latestCurrencyRatesUseCase
                .execute(GetLatestCurrencyRatesPeriodicallyUseCase.Params(selectedCurrency))
                .safeCollect {
                    when (it) {
                        is Resource.Success -> {
                            _isLoading.postValue(false)
                            updateCurrenciesState(it.data)
                        }
                        is Resource.Error -> {
                            _isLoading.postValue(false)
                            _error.postValue(it.error)
                        }
                        is Resource.Loading -> {
                            _isLoading.postValue(true)
                        }
                    }
                }
        }
    }

    fun updateSelectedCurrency(currency: CurrencyModel) {
        _selectedCurrency.postValue(currency)
        updateCurrenciesState(currency.value ?: 0.0)
    }

    fun pauseRefresh() {
        latestCurrencyRatesJob?.cancel()
        latestCurrencyRatesJob = null
    }

    fun resumeRefresh() {
        if (latestCurrencyRatesJob == null) {
            fetchCurrencies()
        }
    }

    private fun updateCurrenciesState(latest: LatestCurrencyModel) =
        viewModelScope.launch(Dispatchers.Default) {
            val currencies = _currencies.value?.toMutableList() ?: mutableListOf()
            val selectedValue = _selectedCurrency.value?.value ?: 0.0
            latest.allCurrencies.forEach { currencyCode ->
                currencies.firstOrNull { it.code == currencyCode }?.let {
                    if (it.code == latest.currency) {
                        it.rate = 1.0
                    } else {
                        it.rate = latest.getRateFor(it.code)
                    }
                    it.convert(selectedValue)
                } ?: run {
                    currencies.add(
                        CurrencyModel(
                            currencyCode,
                            0.0,
                            latest.getRateFor(currencyCode)
                        )
                    )
                }
            }
            _currencies.postValue(currencies)
        }

    private fun updateCurrenciesState(selectedValue: Double) =
        viewModelScope.launch(Dispatchers.Default) {
            val currencies = _currencies.value?.toMutableList() ?: mutableListOf()
            currencies.forEach {
                it.convert(selectedValue)
            }
            _currencies.postValue(currencies)
        }
}