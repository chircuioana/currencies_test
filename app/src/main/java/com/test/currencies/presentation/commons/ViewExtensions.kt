package com.test.currencies.presentation.commons

import android.view.View
import android.widget.EditText

fun EditText.updateTextIfDistinct(newText: String) {
    val text = text?.toString() ?: ""
    if (text != newText) {
        setText(newText)
    }
}

fun View.goneUnless(expression: Boolean) {
    visibility = if (expression) {
        View.VISIBLE
    } else {
        View.GONE
    }
}