package com.test.currencies.presentation

import android.os.Bundle
import android.view.View
import com.test.currencies.presentation.features.currencies.CurrenciesFragment
import com.test.currencies.R
import com.test.currencies.databinding.ActivityMainBinding
import com.test.currencies.presentation.commons.BaseActivity
import javax.inject.Inject

class MainActivity @Inject constructor() : BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun layoutBinding(): View {
        binding = ActivityMainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainerView, CurrenciesFragment())
            .commit()
    }
}