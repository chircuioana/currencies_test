package com.test.currencies.presentation.commons

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.test.currencies.domain.commons.ErrorEntity

abstract class BaseViewModel : ViewModel() {
    val isLoading: LiveData<Boolean> get() = Transformations.distinctUntilChanged(_isLoading)
    protected val _isLoading = MutableLiveData<Boolean>()

    val error: LiveData<ErrorEntity> get() = _error
    protected val _error = SingleLiveEvent<ErrorEntity>()
}