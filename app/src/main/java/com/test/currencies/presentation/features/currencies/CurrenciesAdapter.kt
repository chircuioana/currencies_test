package com.test.currencies.presentation.features.currencies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.test.currencies.databinding.ItemCurrencyBinding
import com.test.currencies.domain.entities.CurrencyModel
import com.test.currencies.presentation.commons.updateTextIfDistinct
import java.util.*

class CurrenciesAdapter(
    val onSelectedCurrencyUpdated: (CurrencyModel) -> Unit
) : RecyclerView.Adapter<CurrenciesAdapter.CurrencyViewHolder>() {

    var currencies = listOf<CurrencyModel>()
        set(value) {
            var animationType: AnimationType = AnimationType.Default()
            if (field.isNotEmpty() && value.isNotEmpty() && field.size == value.size) {
                if (field[0].code != value[0].code) {
                    animationType = AnimationType.Move(field.indexOf(value[0]))
                } else {
                    animationType = AnimationType.UpdateConverts()
                }
            }
            field = value
            when (animationType) {
                is AnimationType.Move -> {
                    notifyItemMoved(animationType.fromPosition, 0)
                    notifyItemRangeChanged(
                        1,
                        value.size - 1,
                        value.subList(1, value.size - 1).map { it.formattedValue })
                }
                is AnimationType.UpdateConverts -> {
                    notifyItemRangeChanged(
                        1,
                        value.size - 1,
                        value.subList(1, value.size - 1).map { it.formattedValue })
                }
                is AnimationType.Default -> {
                    notifyDataSetChanged()
                }
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        return CurrencyViewHolder(
            ItemCurrencyBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: CurrencyViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        if (payloads.isEmpty()) {
            holder.bind(currencies[position])
            return
        }

        (payloads[0] as? List<String>)?.let {
            if (position >= it.size) return@let
            holder.updateValue(it[position - 1])
        }
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        holder.bind(currencies[position])
    }

    override fun getItemCount(): Int = currencies.size

    inner class CurrencyViewHolder(private val itemBinding: ItemCurrencyBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(currency: CurrencyModel) = itemBinding.run {
            tvCurrencyCode.text = currency.code.toUpperCase(Locale.getDefault())
            tvCurrencyName.text = currency.currencyDisplayName.capitalize(Locale.getDefault())
            tvFlagEmoji.text = currency.flagEmoji

            etValue.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    onSelectedCurrencyUpdated(currency.apply {
                        rate = 1.0
                    })
                }
            }

            etValue.doOnTextChanged { text, _, _, _ ->
                if (etValue.hasFocus()) {
                    onSelectedCurrencyUpdated(currency.apply {
                        value = text?.toString()?.toDoubleOrNull() ?: 0.0
                        rate = 1.0
                    })
                }
            }
            etValue.updateTextIfDistinct(currency.formattedValue)
        }

        fun updateValue(formattedValue: String) = itemBinding.run {
            etValue.updateTextIfDistinct(formattedValue)
        }
    }
}


sealed class AnimationType() {
    class Move(val fromPosition: Int) : AnimationType()
    class UpdateConverts() : AnimationType()
    class Default() : AnimationType()
}
